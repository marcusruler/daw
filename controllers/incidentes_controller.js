const Incidente = require('../models/incidente');

exports.getNuevoIncidente = (request, response, next) => {
    response.render('nuevo_incidente', { 
        titulo: "Nuevo incidente" 
    });
};

exports.get = (request, response, next) => {

    response.setHeader('Set-Cookie', 'Jurassic_Park=El lugar donde nada puede malir sal; HttpOnly');

    Incidente.fetchAll().then(([rows, fieldData]) => {
        response.render('incidentes', { 
            incidentes: rows, 
            titulo: "Incidentes",
    })
    .catch(err => {
    console.log(err);
    });

    
    });
}

exports.postNuevoIncidente = (request, response, next) => {

    console.log(request.body.lugar);
    console.log(request.body.tipo);
    
    const incidente = new Incidente(request.body.lugar, request.body.tipo);
    //const incidentetipo = new Incidente(request.body.tipo);


    incidente.save()
    .then(() => {
        response.redirect('/incidentes');
    }).catch(err => {
        console.log(err);
        response.redirect('/incidentes/nuevo-incidente');
    })

    response.redirect('/incidentes');
}