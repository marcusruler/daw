console.log("Hola mundo")
console.log("nodemon es un loquillo")
console.log("cualquier cambio, se ejecuta luego luego")


const express = require('express');
const app = express();

const bodyParser = require('body-parser');

//Middleware
app.use((request, response, next) => {
    console.log('Middleware!');
    next(); //Le permite a la petición avanzar hacia el siguiente middleware
});

app.use('/nueva-mascota', (request, response, next) => {
    response.send('<form action="/guardar" method="POST"> <input type="text name="nombre"><input type="sum">')
});

app.use('/ruta/tachoshino', (request, response, next) => {
    response.send('Respuesta de la ruta "/ruta"'); 
});

app.use('/ruta', (request, response, next) => {
    response.send('Respuesta de la ruta "/ruta"'); 
});

app.use((request, response, next) => {
    console.log('Otro middleware!');
    response.send('¡Hola mundo!'); //Manda la respuesta

});


app.listen(3000);