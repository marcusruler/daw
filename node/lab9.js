//funcion del arreglo/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*No la deje dentro de una funcion por que no logro que node la llame dentro de una funcion, pasa lo mismo con las demas funciones*/ 
/*Una función que reciba un arreglo de números y devuelva su promedio.*/
let arreglo = [10,7,8,9,5];

let acum = 0;
let prom = 0;

for(let i=0;i<5;i++)
{
    acum= acum+ arreglo[i];
}

prom= acum/5;

console.log('El promedio del arreglo es: '+ prom);


///funcion del string/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*Una función que reciba un string y escriba el string en un archivo de texto*/
const readline = require('readline');

let interfazCaptura = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const file_system = require('fs');

interfazCaptura.question('Introduce el texto que deseas agregar al documento: ', function(respuesta){
    file_system.writeFileSync("archivo.txt", respuesta);
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*Sigue la demostración del profesor en la sesión de clase sobre los ejemplos básicos para crear un servidor web que se ejecute sobre node, reciba peticiones de un cliente, y le responda.*/ 
const http = require('http');

const server = http.createServer((request, response)=>{
   
    response.write('<html>')
    response.write('<head>')
    response.write('<h1>Interconexion de redes</h1>')
    response.write('<h3>Esta pagina te ayudara a determinar  que mascara deberas utilziar dependiendo de que clase sea</h3>')
   
    response.write('</head>')
    response.write('<body>')
    let val;

    const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
    });
    
    
     readline.question('Introduce que clase es tu red:  ', val => {
    
    
        if(val=='A' || val=='a'){
            console.log('Al ser de clase A, su mascara es 255.0.0.0')
        }else if(val=='B' || val=='b'){
            console.log('Al ser de clase B, su mascara es 255.255.0.0')
        }else if(val=='C' || val=='c'){
            console.log('Al ser de clase C, su mascara es 255.255.255.0')
        }else{
            console.log('La informacion que proporcionas no se puede utilizar')
        }
        readline.close();
    });
    response.write('</body>')
    response.write('</html>')
})

server.listen(3000);

///Pestaña del laboratorio 6//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*Crea una pequeña aplicación web que al enviar una petición al servidor, devuelva una de las páginas que creaste anteriormente en tus laboratorios*/ 
fs = require('fs');

fs.readFile('./Lab6_1.html', function (mipag, html) {
         
    http.createServer(function(request, response) {  

        response.write(html);  
        response.end();  

    }).listen(4000);
});