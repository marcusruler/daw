
function Funcion1()
{
    let n = prompt("Introduce un numero para saber los cuadrados y cubos de las tablas que contiene: ");
    let i = 1;

    let cuadrado = 0;
    let cubo = 0;

    for(;i<=n;i++)
    {
        cuadrado=i*i;
        cubo=i*i*i;
        document.write("El cuadrado de " + i + " es " + cuadrado +", y su cubo es "+ cubo + "<br>");
    }
}

function Funcion2()
{
    let tiempo = 0
    let inicio = Date.now()
    let x = aleatorio()
    let y = aleatorio()
    z = x + y;
    valor=prompt("¿Cuanto es " + x + " + " + y + "?");
    let fin = Date.now()
    if(valor==z)
    {
        document.write("Tu resultado es correcto " + x + " + " + y + " es " + z + "<br>" );
        tiempo = (fin - inicio)/1000
        document.write("Te tomo un total de "+tiempo+" segundos")
    }else
    {
        document.write("Tu resultado es incorrecto " + x + " + " + y + " es " + z + " no " + valor + "<br>" );    
        tiempo = (fin - inicio)/1000
        document.write("Te tomo un total de "+tiempo+" segundos")
    }
}

function aleatorio() 
{
    inferior=0;
    superior=100
    var numPosibilidades = superior - inferior;
    var aleatorio = Math.random() * (numPosibilidades + 1);
    aleatorio = Math.floor(aleatorio);
    return inferior + aleatorio;
}

function Funcion3()
{
    let negativo=0;
    let cero=0;
    let positivo=0;

    let nuevo_arreglo = new Array();

    for(let i=0;i<10;i++)
    {
        nuevo_arreglo[i]=prompt("Introduce el valor del arreglo numero "+ (i+1) +": ")
    }

    for(let i=0;i<10;i++)
    {
        if(nuevo_arreglo[i]<0){
            negativo++;
        }else if(nuevo_arreglo[i]==0){
            cero++;
        }else{
            positivo++;
        }
    }
    document.write("Cuentas con " + negativo + " numeros menores que cero, " + cero + " ceros y " + positivo + " numeros mayores que cero <br>")
}

function Funcion4()
{
    let arreglo1 = new Array()
    let promedio = new Array ()
    let acumulador = 0
    promedio[0]=0
    promedio[1]=0
    promedio[2]=0

    for(let i = 0; i < 3 ; i++)
    {
        acumulador=0
        arreglo1[i]= new Array(3)
        for(let j = 0; j < 3; j++)
        {
            arreglo1[i][j] = prompt("Introduce el valor del arreglo en la posicion [" + (i+1) + "][" + (j+1) + "]")
            acumulador=parseFloat(arreglo1[i][j])
            promedio[i]=acumulador+promedio[i]
        }
        promedio[i]=promedio[i]/3
    }
    for(let i=0;i<3;i++)
    {
        document.write("El promedio de la fila numero "+(i+1)+" de la matriz es: "+promedio[i]+ "<br>")
    }
    
}

function Funcion5()
{
    var cantidad=prompt("Introduce el numero que deseas invertir")
    valor=Number(cantidad.toString().split('').reverse().join(''));
    document.write("Tu numero "+cantidad+" al reves es "+valor);
}

function Funcion6()
{
    var valor=prompt("Introduce el valor de tu red: ")
    if(valor>=1 && valor <=127)
    {
        document.write("Pertenece a la clase A <br>")
        document.write("Su mascara es 255.0.0.0")
    }
    else if(valor>=128 && valor <=191)
    {
        document.write("Pertenece a la clase B <br>")
        document.write("Su mascara es 255.255.0.0")
    }
    else if(valor>=192 && valor <=223)
    {
        document.write("Pertenece a la clase C <br>")
        document.write("Su mascara es 255.255.255.0")
    }
    else if(valor>=224 && valor <=239)
    {
        document.write("Pertenece a la clase D")
    }
    else if(valor>=240 && valor <=255)
    {
        document.write("Pertenece a la clase E")
    }
}
