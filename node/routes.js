
const file_system = require('fs');

const perros = [
    {nombre: 'Origen', imagen: "https://consultorsalud.com/wp-content/uploads/2021/02/La-OMS-concluye-que-el-Covid-19-es-de-origen-animal-y-aparecio-en-diciembre-de-2019.jpg"},
    {nombre:'Sintomas', imagen: "https://i.ytimg.com/vi/maDzCv-OFgs/maxresdefault.jpg"}, 
    {nombre:'Tratamiento', imagen:"https://cadime.es/images/documentos_archivos_web/Noticias_y_Destacado/22796/Tabla_5_Indicaciones_Tratamiento_COVID-19.JPG"}, 
    {nombre:'Mortalidad', imagen: "http://cdn.statcdn.com/Infographic/images/normal/20867.jpeg"}, 
   ];

   const requestHandler = (request, response)=>{
    
    console.log(request.url);

    

    if(request.url === '/'){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Covid 19</title></head><body>');
        response.write('<h1>Bienvenido a la pagina sobre el SARS-COV2</h1>');
        response.write('<ul>')
        for(let perro of perros)
        {
            response.write('<li><a href="'+perro.nombre+'" >'+ perro.nombre+'</a></li>')
        }
        response.write('</ul>')
        response.write('<p>Si accedes a la ruta "/experiencia", podras registrar tu experiencia con el SARS-COV2 y asi apoyar a las personas que se encuentren en esta sitaucion</p>');
        response.write('</body></html>');
    }else if(request.url === '/Origen'){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>perros[0].nombre</title></head><body>');
        response.write('<h1>'+perros[0].nombre+'</h1>');
        response.write('<p>El coronavirus o SARS-CoV-2, es una enfermedad viral respiratoria perteneciente a la misma familia genética de los virus SARS-CoV y MERS-CoV, surgidos en China y Arabia Saudí en los años 2002 y 2012 respectivamente. El primero tiene su origen en los murciélagos, mientras que el MERS-CoV provino de los camellos o dromedarios, quienes a su vez transmitieron el virus a los humanos por zoonosis</p>');
        response.write('<img alt="Imagen de Helga" src="'+perros[0].imagen+'">');
        response.write('</body></html>');
        response.end();
    }else if(request.url === '/Sintomas'){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>perros[1].nombre</title></head><body>');
        response.write('<h1>'+perros[1].nombre+'</h1>');
        response.write('<p>La COVID-19 afecta de distintas maneras en función de cada persona. La mayoría de las personas que se contagian presentan síntomas de intensidad leve o moderada, y se recuperan sin necesidad de hospitalización.</p>');
        response.write('<img alt="Imagen de Helga" src="'+perros[1].imagen+'">');
        response.write('</body></html>');
        response.end();
    }else if(request.url === '/Tratamiento'){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>perros[2].nombre</title></head><body>');
        response.write('<h1>'+perros[2].nombre+'</h1>');
        response.write('<p>Si se siente enfermo debe descansar, beber mucho líquido y comer alimentos nutritivos. Permanezca en una habitación separada de los demás miembros de la familia y utilice un baño exclusivamente para usted si es posible. Limpie y desinfecte frecuentemente las superficies que toque.</p>');
        response.write('<img alt="Imagen de Alacran" src="'+perros[2].imagen+'">');
        response.write('</body></html>');
        response.end();
    }else if(request.url === '/Mortalidad'){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>perros[3].nombre</title></head><body>');
        response.write('<h1>'+perros[3].nombre+'</h1>');
        response.write('<p>Los resultados mostraron que la mortalidad por debajo de los 50 años fue muy baja (< 1%) pero se incrementó exponencialmente a partir de esta edad, especialmente a partir de los 60 años. </p>');
        response.write('<img alt="Imagen de Fiona" src="'+perros[3].imagen+'">');
        response.write('</body></html>');
        response.end();
    }else if(request.url === '/experiencia' && request.method === "GET"){
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Experiencia</title></head><body>');
        response.write('<h1>Experiencias con el SARS-COV2 o Covid 19</h1>');
        response.write('<h2>Por favor escribe en el primer espacio tu nombre y en el segundo tu experiencia</h2>');
        response.write('<form action="/experiencia" method="POST">')
        response.write('Nombre: <input type="text" name="nombre"><br>')
        response.write('Experiencia: <input type="text" name="Experiencia"><br>')
        response.write('<input type="submit" name="enviar" value="Enviar"><br>')
        response.write('</form>')
        response.write('</body></html>');
        response.end();
    }else if(request.url === '/experiencia' && request.method === "POST"){
        const Datos = [];
        request.on('data',(dato)=>{
            console.log(dato);
            Datos.push(dato);
        });

        request.on('end',()=>{
            const datos_completos = Buffer.concat(Datos).toString();
            console.log(datos_completos);
            const nombre_nuevo_perro = datos_completos.split('=')[1].split('&')[0];
            const nueva_experiencia = datos_completos.split('=')[2].split('&')[0];
            perros.push({nombre: nombre_nuevo_perro, experiencia: nueva_experiencia});
            console.log(perros);
            response.statusCode=302;
            file_system.writeFileSync("Nuevas_experiencias.txt", nueva_experiencia);
            response.setHeader('Location','/');
            response.end();
        })
        
        
    }else{
        response.statusCode = 404;
        response.setHeader('Content-Type', 'text/html')
        response.write('<html>');
        response.write('<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Cura</title></head><body>');
        response.write('<h1>Esta pagina aun no esta habilitada</h1>');
        response.write('</body></html>');
        response.end();
    }
    
    
};

module.exports = requestHandler;