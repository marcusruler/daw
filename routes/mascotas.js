
const express = require('express');
const router = express.Router();

const path = require('path');

const isAuth = require('../util/is-auth');

const mascotasController = require('../controllers/mascotas_controller');

router.use('/tolouse',(request, response, next)=>{
response.write('<h1>Esta es la pagina de Tolouse</h1>');
response.write('<p>Tolouse es un pastor aleman muy consentido, le encanta jugar y hacer amigos</p>');
response.write('<img src="https://www.hola.com/imagenes/estar-bien/20191004150785/pastor-aleman-raza-de-perro-caracteristicas/0-728-57/raza-de-perro-pastor-aleman-m.jpg">')
});

router.use('/batman',(request, response, next)=>{
    response.write('<h1>Esta es la pagina de Batman</h1>');
    response.write('<p>Batman es un chihuahua muy serio, le gusta contemplar el abismo y perseguir al gato Joker</p>');
    response.write('<img src="https://i.pinimg.com/originals/ec/a3/cd/eca3cd7ef28c7b97a6a4df5ebc2a3aee.jpg">')
});

router.use('/bebote',(request, response, next)=>{
    response.write('<h1>Esta es la pagina de Bebote</h1>');
    response.write('<p>Bebote es un Bulldog Ingles, le gusta comer galletas de vainilla y que lo carguen</p>');
    response.write('<p>Advertencia: Si abres la puerta correra lo mas lejos que pueda, le gusta que lo persigan</p>')
    response.write('<img src="https://i.pinimg.com/564x/c1/a4/a3/c1a4a36072681fe36f5cd630793114e6.jpg">')
});

router.use('/quelat',(request, response, next)=>{
    response.write('<h1>Esta es la pagina de Quelat</h1>');
    response.write('<p>Quelat es muy educada y protectora, le encanta jugar con el frisbee</p>');
    response.write('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Labrador_Retriever_%281210559%29.jpg/1200px-Labrador_Retriever_%281210559%29.jpg">')
});

router.use('/toribio',(request, response, next)=>{
    response.write('<h1>Esta es la pagina de Toribio</h1>');
    response.write('<p>Toribio es un Gran Danes, le encanta dormir y comer, sin duda la mejor compañia si te gusta la tranquilidad y seguridad</p>');
    response.write('<img src="https://www.zooplus.es/magazine/wp-content/uploads/2017/10/deutsche-dogge.jpg">')
});

router.get('/nueva-mascota', isAuth,mascotasController.getNuevaMascota);

router.post('/nueva-mascota', isAuth,mascotasController.postNuevaMascota);

router.get('/', isAuth, mascotasController.get);

router.get('/:mascota_id', isAuth, mascotasController.getMascota);

router.post('/eliminar', isAuth, mascotasController.postMascota);

module.exports = router;