const express = require('express');
const router = express.Router();

const path = require('path');



const incidentesController = require('../controllers/incidentes_controller');

router.get('/nuevo-incidente', incidentesController.getNuevoIncidente);

router.post('/nuevo-incidente', incidentesController.postNuevoIncidente);

router.get('/', incidentesController.get);

module.exports = router;



