
const express = require('express');
const routerAdop = express.Router();

const mascotasController = require('../controllers/mascotas_controller');

const path = require('path');

routerAdop.get('/adoptado', mascotasController.getMascotaAdoptada);

routerAdop.post('/fueadoptado', mascotasController.postMascotaAdoptada);

module.exports = routerAdop;
