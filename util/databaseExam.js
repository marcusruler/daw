const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'jurassic_park',
    password: ''
});

module.exports = pool.promise();