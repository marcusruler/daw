const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'daw_bd_2021',
    password: ''
});

module.exports = pool.promise();