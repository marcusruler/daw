const db = require('../util/databaseExam');

module.exports = class Incidente {

    //Constructor de la clase. Sirve para crear un nuevo objeto, y en él se definen las propiedades del modelo
    constructor(lugar, tipo) {
        this.lugar = lugar;
        this.tipo = tipo;
        //this.direccion = '#';
        //this.fecha = new Date().toLocaleDateString('ES');
    }

    //Este método servirá para guardar de manera persistente el nuevo objeto. 
    save() {
        return db.execute('INSERT INTO registro (lugar, tipo) VALUES ( ?, ?, ?)',
        [this.lugar, this.tipo]
    );
    }

    //Este método servirá para devolver los objetos del almacenamiento persistente.
    static fetchAll() {
        return db.execute('SELECT * FROM registro');        
    }

    static fetchOne(id){
        return db.execute('SELECT * FROM registro WHERE id = ?', [id]);
    }

    static delete(id) {
        return db.execute('DELETE FROM registro WHERE id = ?', [id]);
    }
}