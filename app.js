
const express = require('express');
const app = express();

const rutasIncidentes = require('./routes/incidentes');

const path = require('path');

const bodyParser = require('body-parser');


var cookieParser = require('cookie-parser');

const session = require('express-session');

const multer = require('multer');

app.use(session({
    secret: 'iujhygfd uhygtfrde ikjuhygtfrdesw kijuhygtfrdes okijuhygtfrdes iuhygtfrdes okijuhygtfrdesw okiujhygtfrde juhygtfrdew', 
    resave: false, //La sesión no se guardará en cada petición, sino sólo se guardará si algo cambió 
    saveUninitialized: false, //Asegura que no se guarde una sesión para una petición que no lo necesita
}));

app.use(express.static(path.join(__dirname, 'public')));

app.set('view engine', 'ejs');
app.set('views', 'views');

//fileStorage: Es nuestra constante de configuración para manejar el almacenamiento
const fileStorage = multer.diskStorage({
        destination: (request, file, callback) => {
            //'uploads': Es el directorio del servidor donde se subirán los archivos 
            callback(null, 'uploads');
        },
        filename: (request, file, callback) => {
            //aquí configuramos el nombre que queremos que tenga el archivo en el servidor, 
            //para que no haya problema si se suben 2 archivos con el mismo nombre concatenamos el timestamp
            callback(null, new Date().getMilliseconds() + '_' + file.originalname);
        },
    });
    

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json())

app.use(multer({ storage: fileStorage }).single('imagen')); 

app.use(cookieParser());

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

//Middleware
app.use((request, response, next) => {
    console.log('Middleware!');
    next(); //Le permite a la petición avanzar hacia el siguiente middleware
});

app.use('/incidentes', rutasIncidentes);

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use((request, response, next) => {
    console.log('Error 404');
    response.status(404);
    response.send('Lo sentimos, no puedes acceder a esta seccion'); //Manda la respuesta
});

app.listen(4000);